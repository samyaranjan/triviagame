import React, { useState, useEffect } from 'react';
import { Text, View, StyleSheet, TextInput, TouchableOpacity, Image, Button, Alert } from 'react-native';
import Constants from 'expo-constants';

export default function App() {
  const [question, setquestion] = useState('');
  const [answer, setanswer] = useState('');
  const [response, setresponse] = useState();
  const [alertmsg, setalertmsg] = useState(0);

  useEffect(() => {
        quizQuestions();
    }, []);

    const quizQuestions = () => {
      (async function () {
            try {
                setanswer({})
                setresponse({})
                setalertmsg(0)
                var url = 'https://jservice.io/api/random';
                fetch(url).then(res => res.json())
                .then(
                    (res) => {
                        setquestion({"qus":res[0].question, "ans": res[0].answer})
                    },
                    (error) => {
                        console.log("Something went wrong! please try again.");
                });
            } catch (e) {
                console.error(e);
            }
        })();
    }

    const checkAnswer = () => {
      if( answer.data == undefined || answer.data == ''){
        setalertmsg(1)
        setresponse()
        return false;
      }
      if( answer.data && answer.data.toLowerCase() == question.ans && question.ans.toLowerCase()){
        setresponse(true)
        setalertmsg(0)
      }else{
        setresponse(false)
        setalertmsg(0)
      }
    }
  return (
    <View style={styles.container}>
      <Text style={styles.title}>WELCOME TO TRIVIA GAME</Text>
      <Text style={styles.question}> 
        <Text style={{fontWeight:'bold'}}>Question: </Text> 
        {question.qus}
      </Text>

      <TextInput 
        style = {styles.input}
        underlineColorAndroid = "transparent"
        placeholder="Write your answer here!"
        onChangeText={(e) => setanswer({ "data": e })}
        value={answer.data}
      />
      {response === true &&
        <View style={{alignItems: "center"}}>
          <Text style={styles.successresponse}>Correct Answer &#10003;</Text>
          <Text>Click on next to move to next question.</Text>
        </View>
      }
      {response === false &&
        <View style={{alignItems: "center"}}>
          <Text style={styles.failresponse}>Incorrect Answer &#10060;</Text>
          <Text>Click on next to move to next question.</Text>
        </View>
      }
      {alertmsg == 1 && <Text>Please write your answer.</Text>}
      <View style={styles.buttonRow}>
        <TouchableOpacity style={styles.next} onPress={quizQuestions}>
          <Text style={styles.btnText}>NEXT</Text>
        </TouchableOpacity>

        <TouchableOpacity style={styles.submit} onPress={checkAnswer}>
          <Text style={styles.btnText}>SUBMIT</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    paddingTop: Constants.statusBarHeight,
    padding: 8,
  },
  title: {
    justifyContent: 'center',
    fontSize: 20,
    fontWeight: '600',
    color: 'blue',
    textAlign: 'center',
    textDecorationLine: "underline",
  },
  question: {
    margin: 18,
    fontSize: 16,
    textAlign: 'center',
  },
  input: {
    margin: 10,
    height: 100,
    borderColor: '#7a42f4',
    borderWidth: 1,
    padding: 10,
    overflow:'scroll',
 },
 successresponse: {
    height: 30,
    color:'green',
    fontWeight:'bold',
  },
  failresponse: {
    height: 30,
    color:'red',
    fontWeight:'bold',
  },
 submit: {
    width: "40%",
    borderRadius: 25,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 20,
    marginRight:5,
    backgroundColor: "#5AC40B",
  },
  btnText:{
    color: '#fff',
  },
  next:{
    width: "40%",
    borderRadius: 25,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 20,
    backgroundColor: "#0E43C8",
  },
  buttonRow:{
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
});
